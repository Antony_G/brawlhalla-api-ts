const	request = require("request"),
	parseXML = require("xml2js").parseString,
	urlValidator = require("url-validator");

const BrawlhallaApi = function(api_key) {
	this.api_key = api_key;
	this.legendsById = [
		null,null,null,
		"Bödvar",
		"Cassidy",
		"Orion", // 5
		"Lord Vraxx",
		"Gnash",
		"Queen Nai",
		"Lucien",
		"Hattori", // 10
		"Sir Roland",
		"Scarlet",
		"Thatch",
		"Ada",
		"Sentinel", // 15
		"Teros",
		null, // 17
		"Ember",
		"Brynn",
		"Asuri", // 20
		"Barraza",
		"Ulgrim",
		"Azoth",
		"Koji",
		"Diana", // 25
		"Jhala",
		null, // 27
		"Kor",
		"Wu Shang",
		"Val", // 30
		"Ragnir",
		"Cross",
		"Mirage",
		"Nix",
		"Mordex", // 35
		"Yumiko",
		"Artemis",
		"Caspian",
		"Sidra",
		"Xull"	// 40
	];
	this.legends = {
		"bodvar": {id: 3, name: "Bödvar", weapon1: "hammer", weapon2: "sword"},
		"cassidy": {id: 4, name: "Cassidy", weapon1: "blasters", weapon2: "hammer"},
		"orion": {id: 5, name: "Orion", weapon1: "lance", weapon2: "spear"},
		"lord_vraxx": {id: 6, name: "Lord Vraxx", weapon1: "lance", weapon2: "blasters"},
		"gnash": {id: 7, name: "Gnash", weapon1: "hammer", weapon2: "spear"},
		"queen_nai": {id: 8, name: "Queen Nai", weapon1: "spear", weapon2: "katar"},
		"hattori": {id: 10, name: "Hattori", weapon1: "sword", weapon2: "spear"},
		"sir_roland": {id: 11, name: "Sir Roland", weapon1: "lance", weapon2: "sword"},
		"scarlet": {id: 12, name: "Scarlet", weapon1: "hammer", weapon2: "lance"},
		"thatch": {id: 13, name: "Thatch", weapon1: "sword", weapon2: "blasters"},
		"ada": {id: 14, name: "Ada", weapon1: "blasters", weapon2: "spear"},
		"sentinel": {id: 15, name: "Sentinel", weapon1: "hammer", weapon2: "katar"},
		"lucien": {id: 9, name: "Lucien", weapon1: "katar", weapon2: "blasters"},
		"teros": {id: 16, name: "Teros", weapon1: "axe", weapon2: "hammer"},
		"brynn": {id: 19, name: "Brynn", weapon1: "axe", weapon2: "spear"},
		"asuri": {id: 20, name: "Asuri", weapon1: "katar", weapon2: "sword"},
		"barraza": {id: 21, name: "Barraza", weapon1: "axe", weapon2: "blasters"},
		"ember": {id: 18, name: "Ember", weapon1: "bow", weapon2: "katar"},
		"azoth": {id: 23, name: "Azoth", weapon1: "bow", weapon2: "axe"},
		"koji": {id: 24, name: "Koji", weapon1: "bow", weapon2: "sword"},
		"ulgrim": {id: 22, name: "Ulgrim", weapon1: "axe", weapon2: "lance"},
		"diana": {id: 25, name: "Diana", weapon1: "bow", weapon2: "blasters"},
		"jhala": {id: 26, name: "Jhala", weapon1: "axe", weapon2: "sword"},
		"kor": {id: 28, name: "Kor", weapon1: "gauntlets", weapon2: "hammer"},
		"wu_shang": {id: 29, name: "Wu Shang", weapon1: "gauntlets", weapon2: "spear"},
		"val": {id: 30, name: "Val", weapon1: "gauntlets", weapon2: "sword"},
		"ragnir": {id: 31, name: "Ragnir", weapon1: "katar", weapon2: "axe"},
		"cross": {id: 32, name: "Cross", weapon1: "blasters", weapon2: "gauntlets"},
		"mirage": {id: 33, name: "Mirage", weapon1: "scythe", weapon2: "spear"},
		"nix": {id: 34, name: "Nix", weapon1: "scythe", weapon2: "blasters"},
		"mordex": {id: 35, name: "Mordex", weapon1: "scythe", weapon2: "gauntlets"},
		"yumiko": {id: 36, name: "Yumiko", weapon1: "bow", weapon2: "hammer"},
		"artemis": {id: 37, name: "Artemis", weapon1: "lance", weapon2: "scythe"},
		"caspian": {id: 38, name: "Caspian", weapon1: "gauntlets", weapon2: "katar"},
		"sidra": {id: 39, name: "Sidra", weapon1: "cannon", weapon2: "sword"},
		"xull": {id: 40, name: "Xull", weapon1: "cannon", weapon2: "axe"}
	};
	this.alias = {
		"bödvar": "bodvar",
		"lord vraxx": "lord_vraxx", "lordvraxx": "lord_vraxx", "vraxx": "lord_vraxx",
		"queen nai": "queen_nai", "queennai": "queen_nai", "nai": "queen_nai",
		"sir roland": "sir_roland", "sirroland": "sir_roland", "roland": "sir_roland",
		"barazza": "barraza", "barrazza": "barraza", "baraza": "barraza",
		"wu shang": "wu_shang", "wu": "wu_shang", "wushang": "wu_shang",
		"wu sheng": "wu_shang", "wusheng": "wu_shang",
		"yamiko": "yumiko",

		"blaster": "blasters", "gun": "blasters", "guns": "blasters", "pistol": "blasters",
		"rocket lance": "lance", "rocketlance": "lance",
		"gauntlet": "gauntlets", "fists": "gauntlets",
		"canon": "cannon"
 	};
	this.weapons = {
		"hammer": "Hammer",
		"sword": "Sword",
		"blasters": "Blasters",
		"lance": "Rocket Lance",
		"spear": "Spear",
		"katar": "Katar",
		"axe": "Axe",
		"bow": "Bow",
		"gauntlets": "Gauntlets",
		"scythe": "Scythe",
		"cannon": "Cannon",
		"unarmed": "Unarmed"
	};
	this.regions = {
		"us-e": "atl",
		"us-w": "cal",
		"eu": "ams",
		"sea": "sgp",
		"aus": "aus",
		"brz": "brs"
	};
	return this;
};

BrawlhallaApi.prototype.callApi = function(call, params = {}) {
	var self = this;
	return new Promise(function(fulfill, reject) {
		params["api_key"] = self.api_key;
		var urlParams = Object.keys(params).map(function(key) {
			return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
		}).join('&');
		let url = "https://api.brawlhalla.com" + call + "?" + urlParams;
		if (url) {
			request({url: url, encoding: "utf8"}, function(error, response, body) {
				if (!error && response.statusCode == 200) {
					body = JSON.parse(body);
					if (body.error) {
						console.log(`${call}: ${body.error.code} - ${body.error.message}`);
						reject(body);
					} else {
						fulfill(body);
					}
				} else {
					reject(error);
				}
			});
		} else {
			reject("Bad api call");
		}
	});
};

BrawlhallaApi.prototype.getSteamId = function(profileUrl) {
	return new Promise(function(fulfill, reject) {
		//var test = /^((http(s?):\/\/)?(www\.)?steamcommunity\.com\/(id|profiles)\/(\S+))/i;
		let test = /(steamcommunity\.com\/(id|profiles)\/([^\s\/]+))/i;
		let match = test.exec(profileUrl);

		if (match) {
			let url = urlValidator("https://" + match[0] + "?xml=1");
			if (url) {
				request(url, function(error, response, body) {
					if (!error && response.statusCode == 200) {
						parseXML(body, function(err, result) {
							if (!err) {
								if (result.profile && result.profile.steamID64) {
									var steamId = {};
									steamId.name = result.profile.steamID[0];
									steamId.id = result.profile.steamID64[0];
									steamId.profile = "https://steamcommunity.com/profile/" + result.profile.steamID64;
									fulfill(steamId);
								} else {
									reject("Not a valid Steam profile.");
								}
							} else {
								reject(err);
							}
						});
					} else {
						reject(error);
					}
				});
			} else {
				reject("Not a valid Steam profile.");
			}
		} else {
			reject("Not a valid Steam profile.");
		}
	});
};

BrawlhallaApi.prototype.getBhidBySteamId = function(steamId) {
	var self = this;
	return new Promise(function(fulfill, reject) {
		var params = {};
		params["steamid"] = steamId;
		self.callApi("/search", params).then(function(response) {
			if ((response.length > 0) || (response.brawlhalla_id)) {
				response["profile"] = "https://steamcommunity.com/profiles/" + steamId;
				response["steamId"] = steamId;
				fulfill(response);
			} else {
				reject("No users found matching that SteamId");
			}
		}, reject);
	});
};

BrawlhallaApi.prototype.getBhidBySteamUrl = function(profileUrl) {
	var self = this;
	return new Promise(function(fulfill, reject) {
		self.getSteamId(profileUrl).then(function(steamId) {
			var params = {};
			params["steamid"] = steamId.id;
			self.callApi("/search", params).then(function(response) {
				if (response.brawlhalla_id) {
					response["profile"] = steamId.profile;
					response["steamId"] = steamId.id;
					fulfill(response);
				} else {
					reject("No Brawlhalla account associated with that Steam ID.");
				}
			}, function(error){console.log("callApi Error:\n" + error); reject(error);});
		}, function(error){console.log("getSteamId Error:\n" + error); reject(error);});
	});
};

BrawlhallaApi.prototype.getPlayerStats = function(bhid) {
	var self = this;
	// /player/{bhid}/stats
	return new Promise(function(fulfill, reject) {
		self.callApi(`/player/${bhid}/stats`).then(fulfill, reject);
	});
};

BrawlhallaApi.prototype.getPlayerRanked = function(bhid) {
	var self = this;
	// /player/{bhid}/ranked
	return new Promise(function(fulfill, reject) {
		self.callApi(`/player/${bhid}/ranked`).then(fulfill, reject);
	});
};

BrawlhallaApi.prototype.getLegendInfo = function(legendId) {
	var self = this;
	// /legend/{legendid}
	if (typeof(legendId) === "string") {
		if (self.alias[legendId.toLowerCase()]) legendId = self.alias[legendId.toLowerCase()];
		if (self.legends[legendId.toLowerCase()]) legendId = self.legends[legendId.toLowerCase()].id;
	}

	return new Promise(function(fulfill, reject) {
		self.callApi(`/legend/${legendId}`).then(fulfill, reject);
	});
};

BrawlhallaApi.prototype.getLegendByName = BrawlhallaApi.prototype.getLegendInfo;

BrawlhallaApi.prototype.getClanStats = function(clanId) {
	var self = this;
	// /clan/{clanid}
	return new Promise(function(fulfill, reject) {
		self.callApi(`/clan/${clanId}`).then(fulfill, reject);
	});
};

BrawlhallaApi.prototype.getRankings = function(boards = {}) {
	var self = this;
	var options = {
		"bracket": "1v1",
		"region": "all",
		"page": 1
	};

	for (x in boards) {
		options[x] = boards[x];
	}
	var params = {};
	if (options.name) { params['name'] = options.name; }
	return new Promise(function(fulfill, reject) {
		self.callApi(`/rankings/${options.bracket}/${options.region}/${options.page}`, params).then(fulfill, reject);
	});
};

BrawlhallaApi.prototype.getBhidByName = function(name) {
	var self = this;
	return new Promise(function(fulfill, reject) {
		var params = {};
		params['name'] = name;
		self.callApi(`/rankings/1v1/all/1`, params).then(function(users){
			var results = [];
			users.forEach(function(user){
				if (decodeURIComponent(escape(user.name)).toLowerCase() == name.toLowerCase()) {
					results.push(user);
				}
			});
			if (results.length == 0) {
				reject("No users found matching " + name);
			} else {
				fulfill(results);
			}
		}, reject);
	});
};

module.exports = function(api_key) {
	return new BrawlhallaApi(api_key);
};
